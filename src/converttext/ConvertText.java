/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converttext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class ConvertText {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String text;
        text = "Возвратить 1коллекцию    слов которые длинне чем средняя длинна слов"
            + " в переданном тексте. При 2этом коллекция должна быть " +
            "отсортирована по длинне слов, но первыми 5должны идти слова с цыфрами";
        String[] words = text.split("\\.|\\s+|\\,|\\t",0);
        List<String> firstList = new ArrayList(Arrays.asList(words));
        
        //Remove empty
        firstList.removeAll(Arrays.asList(""));
        
        //Get mean length of words
        int mean = 0;
        Iterator<String> iterator = firstList.iterator();
        while(iterator.hasNext()){
            String t = (String) iterator.next();
            mean += t.length();
        }
        mean = mean/firstList.size();
        
        //Create list of words with which length more than mean
        iterator = firstList.iterator();
        List<String> onlyMeanWords = new ArrayList<>();
        List<String> onlyD = new ArrayList<>();
        List<String> onlyC = new ArrayList<>();
        while(iterator.hasNext()){
            String t = (String) iterator.next();
            if(t.length() > mean){
                if(t.substring(0, 1).chars().allMatch(Character::isDigit)){
                    onlyD.add(t);
                }else{
                    onlyC.add(t);
                }
            }
        }
        
        //Sort list
        onlyD.sort(Comparator.comparing(String::length));
        onlyC.sort(Comparator.comparing(String::length));
//        onlyMeanWords.sort(Comparator.comparing(String::length));
        onlyMeanWords = onlyD;
        onlyMeanWords.addAll(onlyC);


//        Comparator<String> compareByLength = new Comparator<String>()
//            {
//                @Override
//                public int compare(String item1, String item2)
//                {
//                    if((item1.substring(0, 1).chars().allMatch(Character::isDigit) && !item2.substring(0, 1).chars().allMatch(Character::isDigit))
//                            ||(!item1.substring(0, 1).chars().allMatch(Character::isDigit) && item2.substring(0, 1).chars().allMatch(Character::isDigit))){
//                        return 1;
//                    }
//                    return Integer.compare(item1.length(), item2.length());
//                }
//            };
//
//        Collections.sort(onlyMeanWords, compareByLength);

        System.out.println("Mean = "+mean);
        System.out.println(firstList);
        System.out.println(onlyMeanWords);
    }
    
}
